package toonly.appobj;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import toonly.configer.watcher.ChangeWatcher;
import toonly.configer.PropsConfiger;
import toonly.configer.cache.UncachedException;
import toonly.debugger.Debugger;

import java.util.Properties;

/**
 * Created by cls on 15-3-16.
 */
public class AppFactory implements ChangeWatcher.ChangeListener {

    private static final Logger log = LoggerFactory.getLogger(AppFactory.class);

    public static final AppFactory instance = new AppFactory();

    private AppFactory() {}

    private static final String APP_CFG = "app.cfg";
    private final PropsConfiger propsConfiger = new PropsConfiger();
    private Properties _config = this.getConfig();

    private Properties getConfig() {
        this.propsConfiger.watch(APP_CFG).AddChangeListener(this);
        try {
            return propsConfiger.cache(APP_CFG);
        } catch (UncachedException e) {
            return propsConfiger.config(APP_CFG);
        }
    }

    public Class<?> getAppClass(String key) {
        String appClassName = this._config.getProperty(key, "java.lang.Object");
        Debugger.debugRun(AppFactory.class, () -> log.info("app class name : {}", appClassName));
        try {
            return AppFactory.class.getClassLoader().loadClass(appClassName);
        } catch (ClassNotFoundException e) {
            return Object.class;
        }
    }

    @Override
    public void onChange() {
        log.info("config [{}] has updated", APP_CFG);
        this._config = this.propsConfiger.config(APP_CFG);
    }

}
