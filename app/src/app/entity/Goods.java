package app.entity;

import toonly.dbmanager.base.*;
import toonly.mapper.ParamConstructable;

/**
 * Created by cls on 15-3-13.
 */
public class Goods implements Addable, Delable, Modable, Selable, Jsonable, ParamConstructable {

    @Column @KeyColumn private String code;
    @Column @DuplicatedColumn private String name;
    @Column @DuplicatedColumn private String color;
    @Column @DuplicatedColumn private String size;

    @Override
    public String getSchemaName() {
        return null;
    }

    @Override
    public String getTableName() {
        return null;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "Goods{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", color='" + color + '\'' +
                ", size='" + size + '\'' +
                '}';
    }
}
