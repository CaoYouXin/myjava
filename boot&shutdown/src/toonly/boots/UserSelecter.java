package toonly.boots;

import com.sun.istack.internal.NotNull;
import toonly.configer.PropsConfiger;
import toonly.configer.cache.UncachedException;
import toonly.dbmanager.lowlevel.DB;
import toonly.dbmanager.lowlevel.RS;
import toonly.dbmanager.sqlbuilder.*;

import java.util.Properties;

/**
 * Created by cls on 15-3-13.
 */
public class UserSelecter {

    static class Ret {
        boolean suc;
        boolean admin;

        public Ret(boolean suc, boolean admin) {
            this.suc = suc;
            this.admin = admin;
        }
    }

    private static final Properties _configs = getConfigs();

    private static Properties getConfigs() {
        PropsConfiger propsConfiger = new PropsConfiger();
        try {
            return propsConfiger.cache("user.cfg");
        } catch (UncachedException e) {
            return propsConfiger.config("user.cfg");
        }
    }

    private static final String _username = _configs.getProperty("username", "username");
    private static final String _password = _configs.getProperty("password", "password");
    private static final String _isAdmin = _configs.getProperty("isAdmin", "isAdmin");

    public static Ret check(@NotNull String username, @NotNull String password) {
        TableId tableId = new TableId(_configs.getProperty("schema", "userdb"), _configs.getProperty("table", "user"));
        PreparedSQL select = new Select(tableId, _username, _password, _isAdmin)
                .where(new Where(new Equal(tableId, _username)));
        RS rs = DB.instance().preparedQuery(select.toPreparedSql(), username);
        while (rs.next()) {
            if (password.equals(rs.getString(_password))) {
                return new Ret(true, rs.getBoolean(_isAdmin));
            }
        }
        return new Ret(false, false);
    }

}
