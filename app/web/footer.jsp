<%--
  Created by IntelliJ IDEA.
  User: cls
  Date: 15-3-13
  Time: 上午8:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Footer -->
<div id="footer">
    <div class="copyrights">Copyright &copy; 2015.CPU All rights reserved.<a target="_blank" href="https://github.com/CaoYouXin/storehouse">项目托管</a></div>
    <ul class="footer-links ">
        <li class="dropup">
            <a class="user-menu" data-toggle="dropdown"><i class="icon-cogs"></i><span>联系管理员<b class="caret"></b></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="#data" class="lightbox"><i class="icon-user"></i>曹同学【开发人员】</a>
                    <div style="display:none"><div id="data">
                        <br>邮箱：c_youxin@163.com
                        <br>电话：18535276856
                    </div></div>
                </li>
            </ul>
        </li>
        <li><a href="" title="" class="user-menu"><i class="icon-screenshot"></i>报告错误</a></li>
    </ul>
</div>
<!-- /footer -->
